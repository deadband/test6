﻿using System.Collections.Generic;

namespace BankAppData.Entity
{
    public class Account : IAccount
    {
        public int Id { get; set; }
        public decimal Balance { get; set; }
        public decimal DebtLimit { get; set; }
        public string Number { get; set; }
        public string Owner { get; set; }
        public IAccount SavingsAccount { get; set; }
        public bool IsDebtGranted { get; set; }
        public IList<Transfer> Transfers { get; set; }
    }
}
