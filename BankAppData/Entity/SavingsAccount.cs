﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAppData.Entity
{
    public class SavingsAccount : IAccount
    {
        public int Id { get; set; }
        public decimal IntesertRate { get; set; }
        public decimal Balance { get; set; }
        public string Number { get; set; }
        public string Owner { get; set; }
    }
}
