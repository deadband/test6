﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAppData.Entity
{
    public class Transfer
    {
        public int Id { get; set; }
        public string SendersAccountNumber { get; set; }
        public string RecipientsAccountNumber { get; set; }
        public DateTime date { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceBefore { get; set; }
        public decimal BalanceAfter { get; set; }
        public TransferType TransferType { get; set; }
    }
}