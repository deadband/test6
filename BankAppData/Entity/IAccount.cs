﻿namespace BankAppData.Entity
{
    public interface IAccount
    {
        decimal Balance { get; set; }
        int Id { get; set; }
        string Number { get; set; }
        string Owner { get; set; }
    }
}